const {createLogger,format,transports} = require("winston");

const logger = createLogger({
  level: process.env.INFO,
  format: format.combine(format.timestamp(), format.json()),
  transports: [
    //
    // - Write all logs with importance level of `error` or less to `error.log`
    // - Write all logs with importance level of `info` or less to `combined.log`
    //
    new transports.File({
      filename: "../logs/error.log",
      level: process.env.ERR,
      format: format.combine(format.timestamp(), format.json()),
    }),
    new transports.File({ filename: "../logs/combined.log" }),
  ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== "production") {
  logger.add(
    new transports.Console({
      format:format.simple(),
    })
  );
}

module.exports = logger;
